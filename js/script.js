let shiftThemeButton = document.querySelector('#theme-shift');

let pageBackground = document.querySelector('.page-cover');

if (localStorage.getItem('bgColor') !== null){
    pageBackground.classList.add(localStorage.bgColor);
}

shiftThemeButton.addEventListener('click', () => {
    if (!localStorage.getItem('bgColor')){
        localStorage.setItem('bgColor', 'new-theme-cover');
        pageBackground.classList.add(localStorage.bgColor);
    } else {
        pageBackground.className = 'page-cover';
        localStorage.removeItem('bgColor');
    }
});

